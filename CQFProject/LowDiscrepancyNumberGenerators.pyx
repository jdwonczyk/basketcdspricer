import math
import numpy as np
import platform
import pandas as pd
import os
from copy import deepcopy

cimport numpy as np

MAXBIT = 30
SobolInitDataFp = os.getcwd() + '/Sobol_Initialisation_Numbers.csv'
SobolInitData = pd.read_csv(SobolInitDataFp)
def ProcessDirections(s):
    st = s.split(' ')
    return np.append(np.array([int(float(a)) for a in st]),np.zeros(shape=(MAXBIT-len(st))))

SobolInitData['v_i'] = SobolInitData['m_i'].map(ProcessDirections)
def Z(): return deepcopy(SobolInitData['Degree'].values)
def tv(): return deepcopy(SobolInitData['v_i'].values)
def iP(): return deepcopy(SobolInitData['iP'].values)

DTYPE = np.int
FDTYPE = np.float
cdef class SobolNumbers:
    cpdef int initialise(self,int d):
        self.b = MAXBIT# As Cython compiler is 32bit atm. #62 if platform.architecture()[0] == '64bit' else 30
        self.MAXDIM = 40
        self.fac = 0.0
        self.Dimensions = d
        self.iGAMMAN = 0
        self.ix = np.zeros(min(self.Dimensions,self.MAXDIM),dtype=DTYPE)
        cdef int l,k,i,lt
        cdef DTYPE_t value
        
        self.initVt = self.ProcessSobolInitialisationNumbers()
        
        self.initDirectionals()
        #Pre generation:
        for i in range(0,500):
            self.Generate()
        return 0

    def ProcessSobolInitialisationNumbers(self):
        _Z = 0
        _Z = Z()[self.MAXDIM-1]
        _tv = tv()[0:self.MAXDIM]
        _tv[0][0:self.b] = 1
        _tvalt = _tv
        self.mdeg=Z()[0:self.MAXDIM]
        self.initP=iP()[0:self.MAXDIM]
        return _tvalt[0:_Z]

    cdef int initDirectionals(self):
        cdef int j,k,l,r,m,j2
        cdef float nV
        cdef np.ndarray[DTYPE_t,ndim=1] includ
        
        if self.initVt[0][0] != 1: 
            return -1
        
        for i in range(2,self.Dimensions+1):
            m = self.mdeg[i-1]
            j = (self.initP[i-1] << 1) + 1 + (2 ** m)
            includ = np.zeros(shape=(m),dtype=DTYPE)
            
            for k in range(m, 0, -1):
                j2 = j // 2
                includ[k - 1] = 1 if j != (2 * j2) else 0    
                j = j2
            
            for j in range(m+1, self.b+1):
                nV = self.initVt[i-1][j-(m+1)]
                r = 1
                for k in range(1,m+1):
                    r*=2
                    if includ[k - 1]:
                        nV = np.bitwise_xor(
                            int(nV), int(r * self.initVt[i - 1][j - k - 1]))
                self.initVt[i-1][j-1] = int(nV)

        l = 1
        for j in range(self.b - 1, 0, -1):
            l *= 2
            for k in range(0,self.Dimensions):
                self.initVt[k][j-1] *= int(l)
        self.fac = 1.0 / (2 * l)


    cpdef np.ndarray[FDTYPE_t,ndim=1] Generate(self):
        self.iGAMMAN += 1
        cdef int im,j,l,k
        im = self.iGAMMAN

        l=1
        i = int(np.floor(self.iGAMMAN))
        while i % 2 != 0:
            l += 1
            i //= 2

        
        if l > self.b:
           ValueError("Max number of bits not big enough for iteration number: {0}".format(self.iGAMMAN))
        
        
        cdef np.ndarray[FDTYPE_t,ndim=1] x = np.zeros(min(self.Dimensions,self.MAXDIM),dtype=float)
        for k in range(1,min(self.Dimensions,self.MAXDIM)+1): 
            x[k-1] = self.ix[k-1]*self.fac
            self.ix[k-1] ^= int(self.initVt[k-1][l-1]) #lhpart of XorSum of (8.23) Jaeckel (simplification of 8.20 by using Gray code as G(n) differs from G(n+1) by the right-most zero bit of n.)
        return x