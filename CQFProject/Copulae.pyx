import math
import numpy as np
import pandas as pd
import pyximport; pyximport.install()
from scipy.stats import norm, t
from Returns import CholeskyDecomp
from Distributions import InvStdCumNormal
from LowDiscrepancyNumberGenerators cimport SobolNumbers
cimport numpy as np
FDTYPE = np.float
cdef np.ndarray[FDTYPE_t,ndim=1] SharedCopulaAlgoWork(P, SobolNumbers LowDiscNumbers):
    if LowDiscNumbers is None:
            raise TypeError
    #Obtain nearest decomposition matrix if P (Correlation matrix) is not +ve definite
    A = CholeskyDecomp(P)
    cdef np.ndarray[FDTYPE_t,ndim=1] LowDiscU 
    cdef np.ndarray[FDTYPE_t,ndim=1] IndptXtilda
    cdef np.ndarray[FDTYPE_t,ndim=1] CorrlelatedX
    #Simulate Uniform random variables, one for each reference name
    LowDiscU = LowDiscNumbers.Generate()
    #translate the uniform random variables to Gaussian r.v.s
    IndptXtilda = np.fromiter(map(lambda u: InvStdCumNormal(u),LowDiscU),dtype=FDTYPE)
    #transfrom to correlated Gaussian r.v.s
    CorrlelatedX = np.matmul(A.transpose(),IndptXtilda)
    return CorrlelatedX

cpdef np.ndarray[FDTYPE_t,ndim=1] MultVarGaussianCopula(P,SobolNumbers LowDiscNumbers):
    cdef np.ndarray[FDTYPE_t,ndim=1] CorrlelatedX
    CorrlelatedX = SharedCopulaAlgoWork(P,LowDiscNumbers)
    #Obtain uniform random variables for each reference name from embedded correlation structure
    cdef np.ndarray[FDTYPE_t,ndim=1] U = norm.cdf(CorrlelatedX)
    return U

cpdef np.ndarray[FDTYPE_t,ndim=1] MultVarTDistnCopula(P,int df,SobolNumbers LowDiscNumbers):
    cdef np.ndarray[FDTYPE_t,ndim=1] CorrlelatedX
    CorrlelatedX = SharedCopulaAlgoWork(P,LowDiscNumbers)
    cdef np.ndarray uRand= np.zeros(df,dtype=float)
    cdef int i
    cdef float epsilon
    for i in range(0,df):
        uRand[i] = math.pow(np.random.uniform(0,1,1),2)
    epsilon = sum(uRand)
    #Obtain uniform random variables for each reference name from embedded correlation structure
    cdef np.ndarray[FDTYPE_t,ndim=1] U = t.cdf(CorrlelatedX/math.sqrt(epsilon/df),df)
    return U

cpdef FDTYPE_t TCopulaDensity(np.ndarray U, int df):
    cdef int i
    cdef np.ndarray[FDTYPE_t,ndim=1] tdistX = np.zeros(shape=(len(U)),dtype=FDTYPE)
    for i in range(0,len(U)):
        tdistX[i]=t.ppf(U[i],df)
    print(tdistX)        
    return t.cdf(tdistX,df)
