#! TO BUILD CYTHON EXTENSIONS, RUN "python setup.py build_ext --inplace" IN A SHELL AT THE PROJECT ROOT. 
#!Ensure that the version of C Compiler you have matches the version used to compile the python environment that you are using.

import pandas as pd
import os
import time
from HazardRates import *
from Returns import *
from EmpiricalFunctions import *
from plotting import Plotter
from Sorting import *
from LowDiscrepancyNumberGenerators import SobolNumbers
from SimulateLegs import TCopula_DF_MLE
from Logger import convertToLaTeX, printf
from MonteCarloCDSBasketPricer import CalculateFairSpreadFromLegs, SimulateCDSBasketDefaultsAndValueLegsGauss, SimulateCDSBasketDefaultsAndValueLegsT, FullMCFairSpreadValuation
from ProbabilityIntegralTransform import *
from scipy.stats import t, expon
from scipy.special import gamma, gammaln
from math import *
import multiprocessing as mp
import random


#Ensure that this code is only run from the main thread:
if __name__ == '__main__':
    cwd = os.getcwd()
    Datafp = cwd + '/FinalProjData.xlsx'
    TenorCreditSpreads = pd.read_excel(Datafp,'TenorCreditSpreads',0)
    HistCreditSpreads = pd.read_excel(Datafp,'HistoricalCreditSpreads',0)
    DiscountCurves = pd.read_excel(Datafp,'DiscountCurves',0)
    plotter = Plotter()
    BPS_TO_NUMBER = 10000

    working_q = mp.Queue()
    output_q = mp.Queue()
        
    t1 = time.time()

    DiscountFactors = dict()
    DiscountFactorCurve = dict()
    for i in range(0,4):
        IndKey = DiscountCurves.columns[2*i + 1]
        IndKeyDate = IndKey + "_Date"
        DiscountFactors[IndKey] = GetDFs(
            TenorCreditSpreads['Tenor'][0:5], 
            TenorCreditSpreads["ToDate"][0], 
            pd.to_datetime(DiscountCurves[IndKeyDate])[DiscountCurves[IndKeyDate].notnull()], 
            DiscountCurves[IndKey][DiscountCurves[IndKey].notnull()]
        )
        DiscountFactorCurve[IndKey] = DiscountFactorFn(DiscountFactors[IndKey])
    plotter.return_lineChart(np.arange(0,5.01,0.01,dtype=np.float),[pd.Series(np.arange(0,5.01,0.01,dtype=np.float)).apply(DiscountFactorCurve["Sonia"])],"Sonia Discount Curve",xlabel="Time/years",ylabel="Discount Factor")
    t2 = time.time()
    print("Took %.10f seconds to Grab Discount Factors." % (t2 - t1))

    DataTenorDic = dict()
    ImpProbDic = dict()
    ImpHazdRts = dict()
    InvPWCDF = dict()
    PWCDF = dict()
    ReferenceNameList = list()
    hazardLines = [[] for i in range(0,5)]
    hazardLegend = ["" for i in range(0,5)]
    for i in range(0,5*5,5):
        IndKey = TenorCreditSpreads['Ticker'][i]
        ReferenceNameList.append(IndKey)
        DataTenorDic[IndKey] = list(TenorCreditSpreads['DataSR'][i:(i+5)] / BPS_TO_NUMBER)
        ImpProbDic[IndKey] = BootstrapImpliedProbalities(0.4,DataTenorDic[IndKey],DiscountFactorCurve["Sonia"])
        Tenors = ImpProbDic[IndKey].index
        BootstrappedSurvProbs = ImpProbDic[IndKey]['ImpliedPrSurv']
        ImpHazdRts[IndKey] = GetHazardsFromP(BootstrappedSurvProbs,Tenors)
        hazardLegend[int(i/5)] = IndKey
        hazardLines[int(i/5)] = [0] + ImpHazdRts[IndKey]
        InvPWCDF[IndKey], PWCDF[IndKey] = ApproxPWCDFDicFromHazardRates(ImpHazdRts[IndKey],0.01)
    plotter.return_lineChart(np.arange(0,6,1,dtype=np.int),np.array(hazardLines),"Hazard rates",xlabel="Time/years",ylabel="Hazard rate", legend=hazardLegend)
    plotter.return_lineChart(np.arange(0,6,1,dtype=np.int),[np.array(ImpProbDic[IndKey]['ImpliedPrSurv']) for IndKey in ReferenceNameList],"Implied default probabilities",xlabel="Time/years",ylabel="Default probability", legend=hazardLegend)
    plotter.return_lineChart([1,2,3,4,5],list(DataTenorDic.values()),name="Credit Spreads", xlabel="Term", ylabel="Spread", legend=list(DataTenorDic.keys()))
    t3 = time.time()

    print("Took %.10f seconds to Grab Tenor Data and Init Inverse Empirical CDF functions." % (t3 - t2))
    
    HistDataDic = dict()
    CanonicalMLETransformedHistDataDic = dict()
    SemiParamTransformedCDFHistDataDic = dict()
    SemiParamTransformedPDFHistDataDic = dict()
    EmpFnForHistSpread = dict()
    SemiParamametric = dict()
    LogReturnsDic = dict()
    DifferencesDic = dict()
    ConsecDiffsDic = dict()

    for i in range(1,6):
        IndKey = HistCreditSpreads.columns[i]
        HistDataDic[IndKey] = pd.DataFrame({ 'Spreads': HistCreditSpreads[IndKey].values/BPS_TO_NUMBER}, dtype=np.float, index = HistCreditSpreads['Date'].values)
        plotter.QQPlot(HistDataDic[IndKey]['Spreads'],"QQ-Plot {0}".format(IndKey))
        LogReturnsDic[IndKey] = LogReturns(HistDataDic[IndKey]['Spreads'],lag=5,jump=5,averageJumpPeriod=True)
        DifferencesDic[IndKey] = AbsoluteDifferences(HistDataDic[IndKey]['Spreads'],jump=5,averageJumpPeriod=True)
        ConsecDiffsDic[IndKey] = AbsoluteDifferences(HistDataDic[IndKey]['Spreads'])
        EmpFnForHistSpread[IndKey] = Empirical_StepWise_CDF(quickSort(DifferencesDic[IndKey].values))
        u_gpdthreashold = np.percentile(HistDataDic[IndKey]['Spreads'],95)
        SemiParamametric[IndKey]= ReturnSemiParametricFitAndAFnToQueuePlot(plotter,list(HistDataDic[IndKey]['Spreads']),u_gpdthreashold, True, "SemiParametricFit_%s"%(IndKey), "Historical Spreads", "Distribution")
        CanonicalMLETransformedHistDataDic[IndKey] = pd.Series(DifferencesDic[IndKey].values).apply(EmpFnForHistSpread[IndKey])
        SemiParamTransformedCDFHistDataDic[IndKey] =  pd.Series(SemiParamametric[IndKey]['%.10f'%(u_gpdthreashold)][0])
        SemiParamTransformedPDFHistDataDic[IndKey] =  pd.Series(SemiParamametric[IndKey]['%.10f'%(u_gpdthreashold)][1])

    #Simulate slightly different historical spreads for sensitivity analysis later on.
    AltHistData_SpreadsOnly = dict()
    AltEmpFnForHistSpread = dict()
    AltSemiParamametric = dict()
    AltLogReturnsDic = dict()
    AltDifferencesDic = dict()
    AltCanonicalMLETransformedHistDataDic = dict()
    AltSemiParamTransformedCDFHistDataDic = dict()
    AltSemiParamTransformedPDFHistDataDic = dict()
    AltEmpFnForHistSpread = dict()
    AltSemiParamametric = dict()
    for i in range(1,6):
        IndKey = HistCreditSpreads.columns[i]
        _forAlthistSpreads = HistDataDic[IndKey]['Spreads']
        AltHistData_SpreadsOnly[IndKey] = np.zeros(shape=(len(_forAlthistSpreads)))
        AltHistData_SpreadsOnly[IndKey][0] = _forAlthistSpreads[0]
        for l in range(1,len(_forAlthistSpreads)):
            AltHistData_SpreadsOnly[IndKey][l] = max([AltHistData_SpreadsOnly[IndKey][l-1]+ConsecDiffsDic[IndKey][l-1]+(expon.rvs(scale=0.00015)*[-1,1][random.randrange(2)]),0.0000000000000001])
        AltLogReturnsDic[IndKey] = LogReturns(pd.Series(AltHistData_SpreadsOnly[IndKey]),lag=5,jump=5,averageJumpPeriod=True)
        AltDifferencesDic[IndKey] = AbsoluteDifferences(pd.Series(AltHistData_SpreadsOnly[IndKey]),jump=5,averageJumpPeriod=True)
        AltEmpFnForHistSpread[IndKey] = Empirical_StepWise_CDF(quickSort(AltDifferencesDic[IndKey].values))
        u_gpdthreashold = np.percentile(AltHistData_SpreadsOnly[IndKey],95)
        AltSemiParamametric[IndKey] = ReturnSemiParametricFitAndAFnToQueuePlot(plotter,list(AltHistData_SpreadsOnly[IndKey]),u_gpdthreashold, True, "SemiParametricFit_%s"%(IndKey), "Alternative Historical Spreads", "Distribution")
        AltCanonicalMLETransformedHistDataDic[IndKey] = pd.Series(AltDifferencesDic[IndKey].values).apply(EmpFnForHistSpread[IndKey])
        AltSemiParamTransformedCDFHistDataDic[IndKey] =  pd.Series(AltSemiParamametric[IndKey]['%.10f'%(u_gpdthreashold)][0])
        AltSemiParamTransformedPDFHistDataDic[IndKey] =  pd.Series(AltSemiParamametric[IndKey]['%.10f'%(u_gpdthreashold)][1])
    
    
    plotter.return_lineChart_dates(HistCreditSpreads['Date'].values,[
        np.array(HistDataDic[HistCreditSpreads.columns[1]]['Spreads'])*BPS_TO_NUMBER,
        np.array(HistDataDic[HistCreditSpreads.columns[2]]['Spreads'])*BPS_TO_NUMBER, 
        np.array(HistDataDic[HistCreditSpreads.columns[3]]['Spreads'])*BPS_TO_NUMBER, 
        np.array(HistDataDic[HistCreditSpreads.columns[4]]['Spreads'])*BPS_TO_NUMBER, 
        np.array(HistDataDic[HistCreditSpreads.columns[5]]['Spreads'])*BPS_TO_NUMBER
        ],name="Historical Credit Spreads Data", xlabel="Historical Date", ylabel="Spread", legend=list(HistCreditSpreads.columns[1:]))

    plotter.return_lineChart_dates(HistCreditSpreads['Date'].values,[
        np.array(AltHistData_SpreadsOnly[HistCreditSpreads.columns[1]])*BPS_TO_NUMBER,
        np.array(AltHistData_SpreadsOnly[HistCreditSpreads.columns[2]])*BPS_TO_NUMBER, 
        np.array(AltHistData_SpreadsOnly[HistCreditSpreads.columns[3]])*BPS_TO_NUMBER, 
        np.array(AltHistData_SpreadsOnly[HistCreditSpreads.columns[4]])*BPS_TO_NUMBER, 
        np.array(AltHistData_SpreadsOnly[HistCreditSpreads.columns[5]])*BPS_TO_NUMBER
        ],name="Alternative Historical Credit Spreads Data", xlabel="Historical Date", ylabel="Spread", legend=list(HistCreditSpreads.columns[1:]))
    
    t4 = time.time()
    print("Took %.10f seconds to grab Historical Spreads and Transform the data by its Empirical CDF." % (t4 - t3))  
    
    plotter.plot_histogram_array(LogReturnsDic, "Weekly Log Returns")
    plotter.plot_histogram_array(DifferencesDic, "Weekly Differences")
    plotter.plot_histogram_array(CanonicalMLETransformedHistDataDic,"Inverse ECDF (Rank)")
    plotter.plot_histogram_array(SemiParamTransformedCDFHistDataDic,"Inverse Semi-Parametric CDF (Rank)")
    plotter.plot_histogram_array(SemiParamTransformedPDFHistDataDic,"Inverse Semi-Parametric PDF (Rank)")

    #try HistoricalSpreads increased by a factor of 10
    Fac10AltHistData_SpreadsOnly = dict()
    Fac10AltEmpFnForHistSpread = dict()
    Fac10AltSemiParamametric = dict()
    Fac10AltLogReturnsDic = dict()
    Fac10AltDifferencesDic = dict()
    Fac10AltCanonicalMLETransformedHistDataDic = dict()
    Fac10AltSemiParamTransformedCDFHistDataDic = dict()
    Fac10AltSemiParamTransformedPDFHistDataDic = dict()
    Fac10AltEmpFnForHistSpread = dict()
    Fac10AltSemiParamametric = dict()
    def times10(x):
        return x * 10
    for i in range(1,6):
        IndKey = HistCreditSpreads.columns[i]
        Fac10AltHistData_SpreadsOnly[IndKey] = HistDataDic[IndKey]['Spreads'].apply(times10)
    
        Fac10AltLogReturnsDic[IndKey] = LogReturns(pd.Series(Fac10AltHistData_SpreadsOnly[IndKey]),lag=5,jump=5,averageJumpPeriod=True)
        Fac10AltDifferencesDic[IndKey] = AbsoluteDifferences(pd.Series(Fac10AltHistData_SpreadsOnly[IndKey]),jump=5,averageJumpPeriod=True)
        Fac10AltEmpFnForHistSpread[IndKey] = Empirical_StepWise_CDF(quickSort(Fac10AltDifferencesDic[IndKey].values))
        u_gpdthreashold = np.percentile(Fac10AltHistData_SpreadsOnly[IndKey],95)
        Fac10AltSemiParamametric[IndKey] = ReturnSemiParametricFitAndAFnToQueuePlot(plotter,list(Fac10AltHistData_SpreadsOnly[IndKey]),u_gpdthreashold, True, "SemiParametricFit_%s"%(IndKey), "Fac 10 Historical Spreads", "Distribution")
        Fac10AltCanonicalMLETransformedHistDataDic[IndKey] = pd.Series(Fac10AltDifferencesDic[IndKey].values).apply(EmpFnForHistSpread[IndKey])
        Fac10AltSemiParamTransformedCDFHistDataDic[IndKey] =  pd.Series(Fac10AltSemiParamametric[IndKey]['%.10f'%(u_gpdthreashold)][0])
        Fac10AltSemiParamTransformedPDFHistDataDic[IndKey] =  pd.Series(Fac10AltSemiParamametric[IndKey]['%.10f'%(u_gpdthreashold)][1])

    t4a = time.time()
    print("Took %.10f seconds to print Transformed Spreads Histograms" % (t4a - t4))


    LogRtnCorP = CorP(LogReturnsDic)
    pdCorLogRtnP = convertToLaTeX(plotter,pd.DataFrame(LogRtnCorP, index = ReferenceNameList, columns=ReferenceNameList, dtype=np.float),name="Pearson Correlation",centerTable=False)
    RankCorP = CorP(CanonicalMLETransformedHistDataDic)
    pdCorRankP = convertToLaTeX(plotter,pd.DataFrame(RankCorP, index = ReferenceNameList, columns=ReferenceNameList, dtype=np.float),name="Rank Correlation",centerTable=False)
    diffCor = RankCorP - LogRtnCorP
    pdCorDiffs = convertToLaTeX(plotter,pd.DataFrame(diffCor, index = ReferenceNameList, columns=ReferenceNameList, dtype=np.float),name="Rank Pearson Correlation Diffs",centerTable=False)

    #!SemiParametric Correlation Matrices
    SemiParametricRankCorP = CorP(SemiParamTransformedCDFHistDataDic)
    pdSemiParametricCorRankP = convertToLaTeX(plotter,pd.DataFrame(SemiParametricRankCorP, index = ReferenceNameList, columns=ReferenceNameList, dtype=np.float),name="Semi Parametric Correlation",centerTable=False)
    diffCorSemi = SemiParametricRankCorP - LogRtnCorP
    pdSemiCorDiffs = convertToLaTeX(plotter,pd.DataFrame(diffCorSemi, index = ReferenceNameList, columns=ReferenceNameList, dtype=np.float),name="Semi Parametric Correlation Diffs",centerTable=False)
    pdRevissedCorDiffs = convertToLaTeX(plotter,pd.DataFrame(SemiParametricRankCorP - RankCorP, index = ReferenceNameList, columns=ReferenceNameList, dtype=np.float),name="Semi Parametric Correlation Diffs From Revision",centerTable=False)

    #UseSemi:
    RankCorP = SemiParametricRankCorP

    #Calculate alternative Hist Data Correlations
    AltLogRtnCorP = CorP(AltLogReturnsDic)
    AltpdCorLogRtnP = convertToLaTeX(plotter,pd.DataFrame(AltLogRtnCorP, index = ReferenceNameList, columns=ReferenceNameList, dtype=np.float),
                                     name="Pearson Correlation from Alternative Historical Data",centerTable=False)
    AltSemiParametricRankCorP = CorP(AltSemiParamTransformedCDFHistDataDic)
    AltpdSemiParametricCorRankP = convertToLaTeX(plotter,pd.DataFrame(AltSemiParametricRankCorP, index = ReferenceNameList, columns=ReferenceNameList,dtype=np.float),
                                                 name="Semi Parametric Correlation from Alternative Historical Data",centerTable=False)
    AltRankCorP = AltSemiParametricRankCorP

    #Calculate Fac 10 alternative Hist Data Correlations
    Fac10AltLogRtnCorP = CorP(AltLogReturnsDic)
    Fac10AltpdCorLogRtnP = convertToLaTeX(plotter,pd.DataFrame(Fac10AltLogRtnCorP, index = ReferenceNameList, columns=ReferenceNameList, dtype=np.float),
                                     name="Pearson Correlation from 10 times Historical Data",centerTable=False)
    Fac10AltSemiParametricRankCorP = CorP(Fac10AltSemiParamTransformedCDFHistDataDic)
    Fac10AltpdSemiParametricCorRankP = convertToLaTeX(plotter,pd.DataFrame(Fac10AltSemiParametricRankCorP, index = ReferenceNameList, columns=ReferenceNameList,dtype=np.float),
                                                 name="Semi Parametric Correlation from 10 times Historical Data",centerTable=False)
    Fac10AltRankCorP = Fac10AltSemiParametricRankCorP

    t5 = time.time()
    print("Took %.10f seconds to calculate Correlation Matrices." % (t5 - t4a))

    U_hist = np.transpose(np.asarray([np.array(ar) for ar in list(SemiParamTransformedCDFHistDataDic.values())]))


    #t distribution degrees of freedom estimation functions.
    def _t2(x,E,v,mu=0):
        '''
        Multivariate t-student density:
        output:
            the density of the given element
        input:
            x = parameter (d dimensional numpy array or scalar)
            mu = mean (d dimensional numpy array or scalar)
            E = scale matrix (dxd numpy array)
            v = degrees of freedom
        '''
        n = E.shape[0]
        if not n == E.shape[1]:
            return IndexError("Scale matrix must be square")
        return pow(np.linalg.det(E),1./2) * \
            gamma((v+n)/2.) / gamma(v/2.) * \
            pow((gamma(v/2.)/gamma((v+1.)/2.)),n) * \
            pow((1+(np.matmul(np.matmul(np.transpose(t.ppf(x,n)),np.linalg.inv(E)),t.ppf(x,v)))/v),-0.5*(v+n)) / \
            np.prod([(pow(1+(pow(t.ppf(xi,v),2))/v,-0.5*(v+1))) for xi in x])

    def T_Cop_DF_MLE(U_hist, corM, maxDF):
        T = U_hist.shape[0]
        D=U_hist.shape[1]
        corInds = np.zeros(shape=(0))
        vE = np.zeros(shape=(maxDF+1))
        nVars = corM.shape[0]
        for i in range(0, corM.shape[0]):
            for j in range(i+1, corM.shape[1]):
                if np.abs(corM[i,j]) > 0.8:
                    if i not in corInds: corInds = np.append(corInds,i)
                    if j not in corInds: corInds = np.append(corInds,j)
                    if i not in corInds and j not in corInds:
                        nVars = nVars - 1
        v = np.max([1,np.min([maxDF, int((nVars *nVars - nVars)/2-1)])])
        vStart = v
        maxV = vStart
        vCounter = 0
        maxSumMLE = float("-inf")
        corMInv = np.linalg.inv(corM)
        detCorM = np.linalg.det(corM)
        while v > 1 and v < (maxDF+1):
            sumMLE = 0.0
            C = np.zeros(shape=U_hist.shape)
            for i in range(0,T):
                for j in range(0,D):
                    C[i,j] = t.ppf(U_hist[i,j],v)
            dum = np.log([_t2(un,corM,v) for un in U_hist])
            sumMLE = sum(dum)
            vE[v] = sumMLE 
            if sumMLE > maxSumMLE:
                maxSumMLE = sumMLE
                maxV = v
            else: vCounter += 1

            if v == maxDF:
                v = vStart -1
                vCounter = 0
            elif v > (vStart - 1):
                v = v+1
            else:
                v = v-1
        
            
            if vCounter > 5:
                if v > (vStart-1): 
                    v = vStart - 1
                    vCounter = 0
                else: 
                    v = 0
        vE[0] = maxV
        return vE




    maxDF = 70
    vE = T_Cop_DF_MLE(U_hist, RankCorP, maxDF)
    plotter.return_lineChart(np.arange(1,maxDF+1),[vE[1:]],name="MLE procedure for T copula degrees of freedom",xlabel="Degrees of Freedom",ylabel="Log-likelihood",trimTrailingZeros=True)
    
    
    #Initialise Sobol Numbers 
    NumbGen = SobolNumbers()
    NumbGen.initialise(LogRtnCorP.shape[0])
    l = 500
    DummyRUs = np.zeros(shape=(l,LogRtnCorP.shape[0]))
    for j in range(0,l):
        DummyRUs[j] = np.array(NumbGen.Generate(),np.float)
    plotter.plot_codependence_scatters(dict(enumerate(DummyRUs.transpose())),"D%","D%") 
    t6 = time.time()
    print("Took %.10f seconds to init and run 5000x%d iterations of sobol numbers." % ((t6 - t5) , LogRtnCorP.shape[0]))



    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!MONTE CARLO SIMULATION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    CDSBasketMaturity = 5.0
    CDSPaymentTenor = 0.25
    CDSPaymentTenors = np.arange(CDSPaymentTenor,CDSBasketMaturity+CDSPaymentTenor,CDSPaymentTenor,dtype=np.float)
    M = BPS_TO_NUMBER

    GaussFairSpread,TFairSpread,t10 = FullMCFairSpreadValuation(plotter,t6,LogRtnCorP,RankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Initial_Fair_Spread_Calculation",plot=True)


    latexFairSpreads = convertToLaTeX(plotter,pd.DataFrame(data=np.array([GaussFairSpread,TFairSpread],dtype=np.float), 
                                                   index = ["Gaussian", "Student's T"], columns=["1st to default","2nd to default","3rd to default","4th to default","5th to default"], dtype=np.float),"Fair Spreads",centerTable=False)

    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,TFairSpread]),
                                    "Comparison of fair spreads",xlabel="K-th to default",ylabel="Fair spread", 
                                    legend=["Gaussian", "Students T"])


results_bank = []
#Define multithreaded worker function.
def worker(working_queue, output_queue, fn_dic, plotter,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE,
                          TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,
                          CDSPaymentTenors,CDSBasketMaturity, 
                          GaussFairSpread,TFairSpread,RankCorP,LogRtnCorP,ReferenceNameList,
                          TweakCDSSpreads,BPS_TO_NUMBER,
                          Fac10AltLogRtnCorP,Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic,
                          AltLogRtnCorP,AltRankCorP,AltSemiParamTransformedCDFHistDataDic):
    '''
    Process each function with the same arg list from end point functions.
    '''
    while True:
        if working_queue.empty() == True:
            break 
        else:
            try:
                picked_fn_name = working_queue.get()
            except:
                print("Something went horribly wrong getting the next worker function")
            else:
                print("Performing work for: {0}".format(picked_fn_name))
                tstart = time.time()
                fn_dic[picked_fn_name](plotter=plotter,M=M,HistCreditSpreads=HistCreditSpreads,SemiParamTransformedCDFHistDataDic=SemiParamTransformedCDFHistDataDic, vE=vE,
                              TenorCreditSpreads=TenorCreditSpreads,InvPWCDF=InvPWCDF,DiscountFactorCurve=DiscountFactorCurve,ImpHazdRts=ImpHazdRts,DataTenorDic=DataTenorDic,
                              CDSPaymentTenors=CDSPaymentTenors,CDSBasketMaturity=CDSBasketMaturity, 
                              GaussFairSpread=GaussFairSpread,TFairSpread=TFairSpread,RankCorP=RankCorP,LogRtnCorP=LogRtnCorP,ReferenceNameList=ReferenceNameList,
                              TweakCDSSpreads=TweakCDSSpreads,BPS_TO_NUMBER=BPS_TO_NUMBER,
                              Fac10AltLogRtnCorP=Fac10AltLogRtnCorP,Fac10AltRankCorP=Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic=Fac10AltSemiParamTransformedCDFHistDataDic,
                              AltLogRtnCorP=AltLogRtnCorP,AltRankCorP=AltRankCorP,AltSemiParamTransformedCDFHistDataDic=AltSemiParamTransformedCDFHistDataDic)
                print("Finished work for: {0} and took {1} mins".format(picked_fn_name,(time.time()-tstart)/60))
                output_queue.put(picked_fn_name)
    return


#define MultiThreaded End Point functions:
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CHECK OF FAIR SPREADS BY MONTE CARLO SIMULATION USING ESTIMATED FAIR SPREAD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def pCheckFairSpead( plotter,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE,
                          TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,
                          CDSPaymentTenors,CDSBasketMaturity, 
                          GaussFairSpread,TFairSpread,RankCorP,LogRtnCorP,ReferenceNameList,
                          TweakCDSSpreads,BPS_TO_NUMBER,
                          Fac10AltLogRtnCorP,Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic,
                          AltLogRtnCorP,AltRankCorP,AltSemiParamTransformedCDFHistDataDic):
    TLegsWFairSpread = SimulateCDSBasketDefaultsAndValueLegsT(plotter,time.time(),RankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,TFairSpread,name="Check_plausability_of_fair_spread")
    GaussLegsWFairSpread = SimulateCDSBasketDefaultsAndValueLegsGauss(plotter,TLegsWFairSpread[2],LogRtnCorP,M,HistCreditSpreads,TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,GaussFairSpread,name="Check_plausability_of_fair_spread")
    t11 = GaussLegsWFairSpread[2]
    TFairDiff = TLegsWFairSpread[0] - TLegsWFairSpread[1] 
    GaussFairDiff = GaussLegsWFairSpread[0] - GaussLegsWFairSpread[1]

    AverageGaussFairDiff = np.array([np.sum(GaussFairDiffD) for GaussFairDiffD in np.transpose(GaussFairDiff)])/GaussFairDiff.shape[0]
    AverageTFairDiff = np.array([np.sum(TFairDiffD) for TFairDiffD in np.transpose(TFairDiff)])/TFairDiff.shape[0]

    convertToLaTeX(plotter,pd.DataFrame(data=np.array([AverageGaussFairDiff,AverageTFairDiff],dtype=np.float), 
                                                    index = ["Gaussian", "Student's T"], 
                                                    columns=["1st to default","2nd to default","3rd to default","4th to default","5th to default"], 
                                                    dtype=np.float),"Averarge CDS leg discrepancy from resimulation of basket using fair spread",centerTable=False)

    gaussCheckFairSpreadDic = dict()
    tCheckFairSpreadDic = dict()
    for iten in range(0,TFairDiff.shape[1]):
        gaussCheckFairSpreadDic["%d-th to default basket CDS" % (iten+1)] = GaussFairDiff[:,iten]
        tCheckFairSpreadDic["%d-th to default basket CDS" % (iten+1)] = TFairDiff[:,iten]
    plotter.plot_histogram_array(gaussCheckFairSpreadDic,"CDS Basket Spreads",name="CDS Basket Outcome Using Gaussian-Copula to check calculated fair spreads")
    plotter.plot_histogram_array(tCheckFairSpreadDic, "CDS Basket Spreads", name="CDS Basket Outcome Using T-Copula to check calculated fair spreads")



#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  RISK AND SENSITIVITY ANALYSIS   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#!Sensitiivity of basket to interest rates------------------------------------------------------------------------------------------
def IRTweak( plotter,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE,
                          TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,
                          CDSPaymentTenors,CDSBasketMaturity, 
                          GaussFairSpread,TFairSpread,RankCorP,LogRtnCorP,ReferenceNameList,
                          TweakCDSSpreads,BPS_TO_NUMBER,
                          Fac10AltLogRtnCorP,Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic,
                          AltLogRtnCorP,AltRankCorP,AltSemiParamTransformedCDFHistDataDic):
    DFTweakFairSpreadGaussList = list()
    DFTweakFairSpreadTList = list()
    DFTweakFairSpreadGaussDiffP = list()
    DFTweakFairSpreadTDiffP = list()
    NonZeroGFAirSpread = np.copy(GaussFairSpread)
    NonZeroGFAirSpread[NonZeroGFAirSpread <= 0] = 0.0000001
    NonZeroTFAirSpread = np.copy(TFairSpread)
    NonZeroTFAirSpread[NonZeroTFAirSpread <= 0] = 0.0000001
    def Tweak_DF(twk):
        TweakedDiscountFactorCurveDic = dict()
        for key in DiscountFactorCurve.keys():
            def f(t):
                return DiscountFactorCurve[key](t) + twk
            TweakedDiscountFactorCurveDic[key] = f
        print("Calculating fair spread after tweaking Discount rates by {0} bps".format(twk*BPS_TO_NUMBER))
        DFTweakFairSpreadGauss,DFTweakFairSpreadT,tdummy = FullMCFairSpreadValuation(plotter,time.time(),LogRtnCorP,RankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],
                                                                                    TenorCreditSpreads,InvPWCDF,TweakedDiscountFactorCurveDic,ImpHazdRts,DataTenorDic,CDSPaymentTenors,
                                                                                    CDSBasketMaturity,name="Discount rates tweaked by {0} bps".format(twk*BPS_TO_NUMBER))
        DFTweakFairSpreadGaussList.append(DFTweakFairSpreadGauss)
        DFTweakFairSpreadTList.append(DFTweakFairSpreadT)
        DFTweakFairSpreadGaussDiffP.append(((DFTweakFairSpreadGauss-GaussFairSpread)/NonZeroGFAirSpread)*100)
        DFTweakFairSpreadTDiffP.append(((DFTweakFairSpreadT-TFairSpread)/NonZeroTFAirSpread)*100)

    irTweaks = np.arange(-500,600,100)/BPS_TO_NUMBER
    for rate_twk in irTweaks:
        Tweak_DF(rate_twk)
    
    DFTweakFairSpreadGaussArr = np.array(DFTweakFairSpreadGaussList)
    DFTweakFairSpreadTArr = np.array(DFTweakFairSpreadTList)

    DFTweakFairSpreadGaussLines = np.transpose(DFTweakFairSpreadGaussArr)
    DFTweakFairSpreadTLines = np.transpose(DFTweakFairSpreadTArr)

    DFTweakFairSpreadGaussArrDiff = np.array(DFTweakFairSpreadGaussDiffP)
    DFTweakFairSpreadTArrDiff = np.array(DFTweakFairSpreadTDiffP)

    DFTweakFairSpreadGaussLinesDiff = np.transpose(DFTweakFairSpreadGaussArrDiff)
    DFTweakFairSpreadTLinesDiff = np.transpose(DFTweakFairSpreadTArrDiff)

    plotter.return_lineChart(irTweaks,DFTweakFairSpreadGaussLines,name="Sensitivity of fair spreads to percentage changes in SONIA rate (Gaussian)",
                        xlabel="Change in SONIA rate",ylabel="Fair spread",legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
    plotter.return_lineChart(irTweaks,DFTweakFairSpreadGaussLinesDiff,name="Sensitivity of percentage changes in fair spreads to changes in SONIA rate (Gaussian)",
                        xlabel="Change in SONIA rate",ylabel="% change in fair spread",legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])

    plotter.return_lineChart(irTweaks,DFTweakFairSpreadTLines,name="Sensitivity of fair spreads to percentage changes in SONIA rate (Students T)",
                        xlabel="Change in SONIA rate",ylabel="Fair spread",legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
    plotter.return_lineChart(irTweaks,DFTweakFairSpreadTLinesDiff,name="Sensitivity of percentage changes in fair spreads to percentage changes in SONIA rate (Students T)",
                        xlabel="Change in SONIA rate",ylabel="% change in fair spread",legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])    


#!Sensitivity of basket to Recovery Rates------------------------------------------------------------------------------------------
def RTweak( plotter,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE,
                          TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,
                          CDSPaymentTenors,CDSBasketMaturity, 
                          GaussFairSpread,TFairSpread,RankCorP,LogRtnCorP,ReferenceNameList,
                          TweakCDSSpreads,BPS_TO_NUMBER,
                          Fac10AltLogRtnCorP,Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic,
                          AltLogRtnCorP,AltRankCorP,AltSemiParamTransformedCDFHistDataDic):
    itera = 0
    Rrng = np.arange(0.0,1.0,0.1)
    GaussFairSpreadTweakR = np.zeros(shape=(len(Rrng),5),dtype=np.float)
    TFairSpreadTweakR = np.zeros(shape=(len(Rrng),5),dtype=np.float)
    tNew = time.time()
    for RAlt in Rrng:
        print("Tweaking recovery rates to rate: {0} and rerunning analysis".format(RAlt))
        GaussFairSpreadTweakR[itera], TFairSpreadTweakR[itera], tNew = FullMCFairSpreadValuation(plotter,time.time(),LogRtnCorP,RankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,RAlt,name="Alternative recovery rate: ({0})".format(RAlt))
        itera += 1
    t18 = tNew
    plotter.return_scatter_multdependencies(Rrng,GaussFairSpreadTweakR.transpose(),"Sensitivity of FairSpread to changing Recovery Rate (Gauss)", 
                        xlabel="Recovery Rate",ylabel="FairSpread",legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
    plotter.return_scatter_multdependencies(Rrng,TFairSpreadTweakR.transpose(),"Sensitivity of FairSpread to changing Recovery Rate (T)", 
                        xlabel="Recovery Rate",ylabel="FairSpread",legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
    
#!Fair spread with Alternative Historical Data------------------------------------------------------------------------------------------
def AltHistDataTweak( plotter,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE,
                          TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,
                          CDSPaymentTenors,CDSBasketMaturity, 
                          GaussFairSpread,TFairSpread,RankCorP,LogRtnCorP,ReferenceNameList,
                          TweakCDSSpreads,BPS_TO_NUMBER,
                          Fac10AltLogRtnCorP,Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic,
                          AltLogRtnCorP,AltRankCorP,AltSemiParamTransformedCDFHistDataDic):
    GaussFairSpreadTweakHistData, TFairSpreadTweakHistData, t16 = FullMCFairSpreadValuation(plotter,time.time(),AltLogRtnCorP,AltRankCorP,M,HistCreditSpreads,
                                                                                                        AltSemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,
                                                                                                        ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,
                                                                                                        name="Alternative Historical Data")
    DeltaGaussFairSpreadTweakHistData = GaussFairSpreadTweakHistData - GaussFairSpread
    DeltaTFairSpreadTweakHistData = TFairSpreadTweakHistData - TFairSpread

    latexAltHistFairSpreads = convertToLaTeX(plotter,pd.DataFrame(data=np.array([GaussFairSpreadTweakHistData,TFairSpreadTweakHistData],dtype=np.float), 
                                                    index = ["Gaussian", "Student's T"], columns=["1st to default","2nd to default","3rd to default","4th to default","5th to default"], dtype=np.float),
                                            "Fair Spreads from alternative term structure of spreads",centerTable=False)


    
def TweakCDSSpreads(TweakIndKey,TweakAmountInBps,TenorCreditSpreads,BPS_TO_NUMBER,DiscountFactorCurve, UseConstantBump=True, UseProduct=False):
    TweakedDataTenorDic = dict()
    TweakedImpProbDic = dict()
    TweakedImpHazdRts = dict()
    TweakedInvPWCDF = dict()
    TweakedPWCDF = dict()
    
    for i in range(0,5*5,5):
        IndKey = TenorCreditSpreads['Ticker'][i]
        TweakedDataTenorDic[IndKey] = np.array(TenorCreditSpreads['DataSR'][i:(i+5)] / BPS_TO_NUMBER)
    for i in range(0,5*5,5):
        IndKey = TenorCreditSpreads['Ticker'][i]
        if IndKey == TweakIndKey or TweakIndKey == "All":
            twk = (TweakAmountInBps if UseConstantBump else expon.rvs(scale=TweakAmountInBps))
            if UseProduct:
                TweakedDataTenorDic[IndKey][0] *= twk
            else:
                TweakedDataTenorDic[IndKey][0] += twk
            for l in range(1,len(TweakedDataTenorDic[IndKey])):
                TweakedDataTenorDic[IndKey][l] += (TweakAmountInBps if UseConstantBump else np.min([expon.rvs(scale=TweakAmountInBps),(TweakedDataTenorDic[IndKey][l-1]-TweakedDataTenorDic[IndKey][l])]))
        TweakedDataTenorDic[IndKey] = list(TweakedDataTenorDic[IndKey])
        TweakedImpProbDic[IndKey] = BootstrapImpliedProbalities(0.4,TweakedDataTenorDic[IndKey],DiscountFactorCurve["Sonia"])
        Tenors = TweakedImpProbDic[IndKey].index
        BootstrappedSurvProbs = TweakedImpProbDic[IndKey]['ImpliedPrSurv']
        TweakedImpHazdRts[IndKey] = GetHazardsFromP(BootstrappedSurvProbs,Tenors)
        TweakedInvPWCDF[IndKey], TweakedPWCDF[IndKey] = ApproxPWCDFDicFromHazardRates(TweakedImpHazdRts[IndKey],0.01)
    return TweakedDataTenorDic, TweakedImpProbDic, TweakedImpHazdRts, TweakedInvPWCDF
        


#!Fair spread after tweaking Credit Spreads and Hist Credit Spreads by a factor of 10------------------------------------------------------------------------------------------
def AllFac10CreditAndHist(plotter, M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE,
                          TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,
                          CDSPaymentTenors,CDSBasketMaturity, 
                          GaussFairSpread,TFairSpread,RankCorP,LogRtnCorP,ReferenceNameList,
                          TweakCDSSpreads,BPS_TO_NUMBER,
                          Fac10AltLogRtnCorP,Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic,
                          AltLogRtnCorP,AltRankCorP,AltSemiParamTransformedCDFHistDataDic):
    GaussFairSpreadTweakCDS = np.zeros(shape=(5),dtype=np.float)
    TFairSpreadTweakCDS = np.zeros(shape=(5),dtype=np.float)

    TweakedDataTenorDic, TweakedImpProbDic, TweakedImpHazdRts, TweakedInvPWCDF = TweakCDSSpreads("All",10,TenorCreditSpreads,BPS_TO_NUMBER,DiscountFactorCurve,True,True)
    LatexTweakedDataTenorDic = convertToLaTeX(plotter,pd.DataFrame.from_dict(TweakedDataTenorDic),name="10 Times Credit Spreads",centerTable=False)
    GaussFairSpreadTweakCDS, TFairSpreadTweakCDS, t17 = FullMCFairSpreadValuation(plotter,time.time(),Fac10AltLogRtnCorP,Fac10AltRankCorP,M,HistCreditSpreads,
                                                                                                        Fac10AltSemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,TweakedInvPWCDF,DiscountFactorCurve,
                                                                                                        TweakedImpHazdRts,TweakedDataTenorDic,CDSPaymentTenors,CDSBasketMaturity,
                                                                                                        name="10 Times Credit Spreads fair spread",plotter=True)

    latexAltFairSpreads = convertToLaTeX(plotter,pd.DataFrame(data=np.array([GaussFairSpreadTweakCDS,TFairSpreadTweakCDS],dtype=np.float), 
                                                    index = ["Gaussian", "Student's T"], columns=["1st to default","2nd to default","3rd to default","4th to default","5th to default"], dtype=np.float),
                                            "Fair Spreads from 10 times term structure of spreads and historical spreads",centerTable=False)

#Tweak Credit Spreads
def CreditSpreadTweak(plotter,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE,
                          TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,
                          CDSPaymentTenors,CDSBasketMaturity, 
                          GaussFairSpread,TFairSpread,RankCorP,LogRtnCorP,ReferenceNameList,
                          TweakCDSSpreads,BPS_TO_NUMBER,
                          Fac10AltLogRtnCorP,Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic,
                          AltLogRtnCorP,AltRankCorP,AltSemiParamTransformedCDFHistDataDic):
        
    #!Fair spread with alternative termstructure of credit spreads all tweaked------------------------------------------------------------------------------------------
    GaussFairSpreadTweakCDS = np.zeros(shape=(5),dtype=np.float)
    TFairSpreadTweakCDS = np.zeros(shape=(5),dtype=np.float)

    TweakedDataTenorDic, TweakedImpProbDic, TweakedImpHazdRts, TweakedInvPWCDF = TweakCDSSpreads("All",0.0002,TenorCreditSpreads,BPS_TO_NUMBER,DiscountFactorCurve,False)
    LatexTweakedDataTenorDic = convertToLaTeX(plotter,pd.DataFrame.from_dict(TweakedDataTenorDic),name="Alternative Term Structure of Credit Spreads",centerTable=False)
    print("Alternative Term Structure of Hazard Rates applied.")
    GaussFairSpreadTweakCDS, TFairSpreadTweakCDS, t17 = FullMCFairSpreadValuation(plotter,time.time(),LogRtnCorP,RankCorP,M,HistCreditSpreads,
                                                                                                        SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,TweakedInvPWCDF,DiscountFactorCurve,
                                                                                                        TweakedImpHazdRts,TweakedDataTenorDic,CDSPaymentTenors,CDSBasketMaturity,
                                                                                                        name="Alternative Term Structure of credit spreads")
    DeltaGaussFairSpreadTweakCDS = GaussFairSpreadTweakCDS - GaussFairSpread
    DeltaTFairSpreadTweakCDS = TFairSpreadTweakCDS - TFairSpread

    latexAltFairSpreads = convertToLaTeX(plotter,pd.DataFrame(data=np.array([GaussFairSpreadTweakCDS,TFairSpreadTweakCDS],dtype=np.float), 
                                                    index = ["Gaussian", "Student's T"], columns=["1st to default","2nd to default","3rd to default","4th to default","5th to default"], dtype=np.float),
                                            "Fair Spreads from alternative term structure of spreads",centerTable=False)

    #!Fair spread by tweaking individual credit spreads to calculate basket delta wrt individual reference names.------------------------------------------------------------------------------------------
    CreditTweaksToCarryOutBps = np.array([-10, -5, 5, 10, 50, 100, 250, 500, 750, 1000]) / BPS_TO_NUMBER

    GaussFairSpreadTweakCDS = np.zeros(shape=(5,5),dtype=np.float)
    TFairSpreadTweakCDS = np.zeros(shape=(5,5),dtype=np.float)
    DeltaGaussFairSpreadTweakCDS = dict()
    DeltaTFairSpreadTweakCDS = dict()
    for CreditTenorTweakAmount in CreditTweaksToCarryOutBps:
        for i in range(0,5*5,5):
            IndKey = TenorCreditSpreads['Ticker'][i]
            if not IndKey in DeltaGaussFairSpreadTweakCDS:
                DeltaGaussFairSpreadTweakCDS[IndKey] = dict()
                DeltaTFairSpreadTweakCDS[IndKey] = dict()
            TweakedDataTenorDic, TweakedImpProbDic, TweakedImpHazdRts, TweakedInvPWCDF = TweakCDSSpreads(IndKey,CreditTenorTweakAmount,TenorCreditSpreads,BPS_TO_NUMBER,DiscountFactorCurve)
            print("Tweaking the credit spreads for {0} and rerunning analysis for tweak: {1}".format(IndKey,CreditTenorTweakAmount))
            GaussFairSpreadTweakCDS[int(i/5)], TFairSpreadTweakCDS[int(i/5)], t17 = FullMCFairSpreadValuation(plotter,time.time(),LogRtnCorP,RankCorP,M,HistCreditSpreads,
                                                                                                                SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,TweakedInvPWCDF,
                                                                                                                DiscountFactorCurve,TweakedImpHazdRts,TweakedDataTenorDic,CDSPaymentTenors,
                                                                                                                CDSBasketMaturity,
                                                                                                                name="Tweak Spread for {0} by {1}".format(IndKey,CreditTenorTweakAmount))
            DeltaGaussFairSpreadTweakCDS[IndKey]["{0}".format(CreditTenorTweakAmount)] = GaussFairSpreadTweakCDS[int(i/5)] - GaussFairSpread
            DeltaTFairSpreadTweakCDS[IndKey]["{0}".format(CreditTenorTweakAmount)] = TFairSpreadTweakCDS[int(i/5)] - TFairSpread
        TweakedDataTenorDic, TweakedImpProbDic, TweakedImpHazdRts, TweakedInvPWCDF = TweakCDSSpreads("All",CreditTenorTweakAmount,TenorCreditSpreads,BPS_TO_NUMBER,DiscountFactorCurve)
        print("Tweaking the credit spreads for All reference names and rerunning analysis for tweak: {0}".format(CreditTenorTweakAmount))
        GaussFairSpreadTweakAllCDS, TFairSpreadTweakAllCDS, t175 = FullMCFairSpreadValuation(plotter,time.time(),LogRtnCorP,RankCorP,M,HistCreditSpreads,
                                                                                                        SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,TweakedInvPWCDF,
                                                                                                        DiscountFactorCurve,TweakedImpHazdRts,TweakedDataTenorDic,CDSPaymentTenors,
                                                                                                        CDSBasketMaturity,
                                                                                                        name="Bump all credit spreads by {0}".format(CreditTenorTweakAmount))
        if not "All" in DeltaGaussFairSpreadTweakCDS:
            DeltaGaussFairSpreadTweakCDS["All"] = dict()
            DeltaTFairSpreadTweakCDS["All"] = dict()
        DeltaGaussFairSpreadTweakCDS["All"]["{0}".format(CreditTenorTweakAmount)] = GaussFairSpreadTweakAllCDS - GaussFairSpread
        DeltaTFairSpreadTweakCDS["All"]["{0}".format(CreditTenorTweakAmount)] = TFairSpreadTweakAllCDS - TFairSpread

    CDSRefNamesArr = np.array(list(TenorCreditSpreads['Ticker'][0:25:5])+["All"])

    for RefName in CDSRefNamesArr:
        print("Calculating Credit Deltas under Gaussian Copula for ReferenceName {0}".format(RefName))
        deltaVBasket = np.transpose(list(DeltaGaussFairSpreadTweakCDS[RefName].values()))
        dBasket = deltaVBasket/CreditTweaksToCarryOutBps
        plotter.return_lineChart(CreditTweaksToCarryOutBps,deltaVBasket,
                            name="Credit Deltas ({0}) (Gauss)".format(RefName),xlabel="Credit Delta", ylabel="Basket Spread Delta",
                            legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
        plotter.return_lineChart(CreditTweaksToCarryOutBps,dBasket,
                            name="dBasketSpread_dCreditSpread ({0}) (Gauss)".format(RefName),xlabel="Credit Delta", ylabel="dV/ds",
                            legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
        fitY = dict()
        xLinY = dict()
        fitD = dict()
        xLinD = dict()
        for k in range(0,len(deltaVBasket)):
            fitYfn, rPowY, xLinY["{0}".format(k)] = plotter.SuitableRegressionFit(CreditTweaksToCarryOutBps,deltaVBasket[k],
                                name="Credit Deltas ({0}) (Gauss) Interpolated".format(RefName),startingPower=3)

            fitDfn, rPowD, xLinD["{0}".format(k)] = plotter.SuitableRegressionFit(CreditTweaksToCarryOutBps,dBasket[k],
                                name="dBasketSpread_dCreditSpread ({0}) (Gauss) Interpolated".format(RefName),startingPower=3)

            fitY["{0}".format(k)] = np.fromiter(map(lambda x: fitYfn(x), xLinY["{0}".format(0)]),dtype=np.float)
            fitD["{0}".format(k)] = np.fromiter(map(lambda x: fitDfn(x), xLinD["{0}".format(0)]),dtype=np.float)
        
        plotter.return_lineChart(xLinY["{0}".format(0)],list(fitY.values()),
                        name="Plot of Interpolated_{1} Credit Deltas for {0} under Gauss Assumption".format(RefName,rPowY),xlabel="Credit Delta", ylabel="Basket Spread Delta",
                        legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
        plotter.return_lineChart(xLinD["{0}".format(0)],list(fitD.values()),
                        name="Plot of Interpolated_{1} dBasketSpread to dCreditSpread for {0} under Gauss Assumption".format(RefName,rPowD),xlabel="Credit Delta", ylabel="dV/ds",
                        legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])

    
        
        print("Calculating Credit Deltas under Students T Copula for ReferenceName {0}".format(RefName))
        deltaVBasket = np.transpose(list(DeltaTFairSpreadTweakCDS[RefName].values()))
        dBasket = deltaVBasket/CreditTweaksToCarryOutBps
        plotter.return_lineChart(CreditTweaksToCarryOutBps,deltaVBasket,
                            name="Credit Deltas ({0}) (T)".format(RefName),xlabel="Credit Delta", ylabel="Basket Spread Delta",
                            legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
        plotter.return_lineChart(CreditTweaksToCarryOutBps,dBasket,
                            name="dBasketSpread_dCreditSpread ({0}) (T)".format(RefName),xlabel="Credit Delta", ylabel="dV/ds",
                            legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
        fitY = dict()
        xLinY = dict()
        fitD = dict()
        xLinD = dict()
        for k in range(0,len(deltaVBasket)):
            fitYfn, rPowY, xLinY["{0}".format(k)] = plotter.SuitableRegressionFit(CreditTweaksToCarryOutBps,deltaVBasket[k],
                                name="Credit Deltas ({0}) (T) Interpolated".format(RefName),startingPower=3)

            fitDfn, rPowD, xLinD["{0}".format(k)] = plotter.SuitableRegressionFit(CreditTweaksToCarryOutBps,dBasket[k],
                                name="dBasketSpread_dCreditSpread ({0}) (T) Interpolated".format(RefName),startingPower=3)

        
            fitY["{0}".format(k)] = np.fromiter(map(lambda x: fitYfn(x), xLinY["{0}".format(0)]),dtype=np.float)
            fitD["{0}".format(k)] = np.fromiter(map(lambda x: fitDfn(x), xLinD["{0}".format(0)]),dtype=np.float)

        plotter.return_lineChart(xLinY["{0}".format(0)],list(fitY.values()),
                        name="Plot of Interpolated_{1} Credit Deltas for {0} under Students T Assumption".format(RefName,rPowY),xlabel="Credit Delta", ylabel="Basket Spread Delta",
                        legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
        plotter.return_lineChart(xLinD["{0}".format(0)],list(fitD.values()),
                        name="Plot of Interpolated_{1} dBasketSpread to dCreditSpread for {0} under Students T Assumption".format(RefName,rPowD),xlabel="Credit Delta", ylabel="dV/ds",
                        legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])




#!Sensitivity of basket to tweaking entire correlation matrix by percent using Fisher Transform.------------------------------------------------------------------------------------------
def CorTweaksAll(plotter, M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE,
                          TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,
                          CDSPaymentTenors,CDSBasketMaturity, 
                          GaussFairSpread,TFairSpread,RankCorP,LogRtnCorP,ReferenceNameList,
                          TweakCDSSpreads,BPS_TO_NUMBER,
                          Fac10AltLogRtnCorP,Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic,
                          AltLogRtnCorP,AltRankCorP,AltSemiParamTransformedCDFHistDataDic):
    CorAllTweakFairSpreadGaussList = list()
    CorAllTweakFairSpreadTList = list()
    CorAllTweakFairSpreadGaussDiffP = list()
    CorAllTweakFairSpreadTDiffP = list()
    def PercTweakCors(percTweak):
        TweakedRankCorP = TweakWhole2DMatrixByPercent(RankCorP,percTweak)
        TweakedLogRtnCorP = TweakWhole2DMatrixByPercent(LogRtnCorP,percTweak)
        convertToLaTeX(plotter,pd.DataFrame(TweakedRankCorP, index = ReferenceNameList, columns=ReferenceNameList, dtype=np.float),
                                        name="Correlation sensitivity Rank Correlation tweaked by {0} percent".format(percTweak),centerTable=False)
        convertToLaTeX(plotter,pd.DataFrame(TweakedLogRtnCorP, index = ReferenceNameList, columns=ReferenceNameList, dtype=np.float),
                                        name="Correlation sensitivity Pearson Correlation tweaked by {0} percent".format(percTweak),centerTable=False)
        print("Calculating fair spread after tweaking correlation for whole matrix by {0}%".format(percTweak*100))
        CorAllTweakFairSpreadGauss,CorAllTweakFairSpreadT,tNew = FullMCFairSpreadValuation(plotter,time.time(),TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                                DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,
                                                                                name="Correlation between all reference names tweaked by {0} percent".format(percTweak*100),plot=True)
        CorAllTweakFairSpreadGaussList.append(CorAllTweakFairSpreadGauss)
        CorAllTweakFairSpreadTList.append(CorAllTweakFairSpreadT)
        NonZeroGFAirSpread = np.copy(GaussFairSpread)
        NonZeroGFAirSpread[NonZeroGFAirSpread <= 0] = 0.0000001
        NonZeroTFAirSpread = np.copy(TFairSpread)
        NonZeroTFAirSpread[NonZeroTFAirSpread <= 0] = 0.0000001
        CorAllTweakFairSpreadGaussDiffP.append(((CorAllTweakFairSpreadGauss-GaussFairSpread)/NonZeroGFAirSpread)*100)
        CorAllTweakFairSpreadTDiffP.append(((CorAllTweakFairSpreadT-TFairSpread)/NonZeroTFAirSpread)*100)

    corPercTweaksArr = [-0.8,-0.5,-0.25, -0.1, 0.0, 0.1, 0.25, 0.5, 0.8]
    for ptw in corPercTweaksArr:
        PercTweakCors(ptw)

    CorAllTweakFairSpreadGaussArr = np.array(CorAllTweakFairSpreadGaussList)
    CorAllTweakFairSpreadTArr = np.array(CorAllTweakFairSpreadTList)

    CorAllTweakFairSpreadGaussLines = np.transpose(CorAllTweakFairSpreadGaussArr)
    CorAllTweakFairSpreadTLines = np.transpose(CorAllTweakFairSpreadTArr)

    CorAllTweakFairSpreadGaussArrDiff = np.array(CorAllTweakFairSpreadGaussDiffP)
    CorAllTweakFairSpreadTArrDiff = np.array(CorAllTweakFairSpreadTDiffP)

    CorAllTweakFairSpreadGaussLinesDiff = np.transpose(CorAllTweakFairSpreadGaussArrDiff)
    CorAllTweakFairSpreadTLinesDiff = np.transpose(CorAllTweakFairSpreadTArrDiff)

    plotter.return_lineChart(corPercTweaksArr,CorAllTweakFairSpreadGaussLines,name="Sensitivity of fair spreads to percentage changes to correlation (Gaussian)",
                        xlabel="Percentage change to correlation",ylabel="Fair spread",legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
    plotter.return_lineChart(corPercTweaksArr,CorAllTweakFairSpreadGaussLinesDiff,name="Sensitivity of percentage changes in fair spreads to percentage changes to correlation (Gaussian)",
                        xlabel="Percentage change to correlation",ylabel="% change in fair spread",legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])

    plotter.return_lineChart(corPercTweaksArr,CorAllTweakFairSpreadTLines,name="Sensitivity of fair spreads to percentage changes to correlation (Students T)",
                        xlabel="Percentage change to correlation",ylabel="Fair spread",legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
    plotter.return_lineChart(corPercTweaksArr,CorAllTweakFairSpreadTLinesDiff,name="Sensitivity of percentage changes in fair spreads to percentage changes to correlation (Students T)",
                        xlabel="Percentage change to correlation",ylabel="% change in fair spread",legend=["1st to default","2nd to default","3rd to default","4th to default","5th to default"])
    
    
#!Sensitivity of basket to tweaking individual pairwise correlations.------------------------------------------------------------------------------------------
def TweakPairwiseCors( plotter,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE,
                          TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,
                          CDSPaymentTenors,CDSBasketMaturity, 
                          GaussFairSpread,TFairSpread,RankCorP,LogRtnCorP,ReferenceNameList,
                          TweakCDSSpreads,BPS_TO_NUMBER,
                          Fac10AltLogRtnCorP,Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic,
                          AltLogRtnCorP,AltRankCorP,AltSemiParamTransformedCDFHistDataDic):
    t19 = time.time()
    print("Tweaking indiviual correlations by individual amounts")
    TweakedRankCorP = Tweak(RankCorP,(1,2),np.min([0.1, 1-RankCorP[1,2]]))
    TweakedLogRtnCorP = Tweak(LogRtnCorP,(1,2),np.min([0.1, 1-LogRtnCorP[1,2]]))
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t20 = FullMCFairSpreadValuation(plotter,t19,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Tweaked Correlation between Barclays & JPMorgan by 0,1")

    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Tweaked Correlation between Barclays & JPMorgan by 0,1 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Tweaked Correlation between Barclays & JPMorgan by 0,1 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])

    TweakedRankCorP = Tweak(RankCorP,(2,4),np.min([0.1, 1-RankCorP[2,4]]))
    TweakedLogRtnCorP = Tweak(LogRtnCorP,(2,4),np.min([0.1, 1-LogRtnCorP[2,4]]))
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t21 = FullMCFairSpreadValuation(plotter,t20,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Tweaked Correlation between JPMorgan & RBS by 0,1")
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Tweaked Correlation between JPMorgan & RBS by 0,1 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Tweaked Correlation between JPMorgan & RBS by 0,1 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])

    TweakedRankCorP = Tweak(RankCorP,(0,3),np.max([-0.1, -1+RankCorP[0,3]]))
    TweakedLogRtnCorP = Tweak(LogRtnCorP,(0,3),np.max([-0.1, -1+LogRtnCorP[0,3]]))
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t22 = FullMCFairSpreadValuation(plotter,t21,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Tweaked Correlation between Deutsche Bank & Goldman Sachs by -0,1")
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Tweaked Correlation between Deutsche Bank & Goldman Sachs by -0,1 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Tweaked Correlation between Deutsche Bank & Goldman Sachs by -0,1 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])

    TweakedRankCorP = Tweak(RankCorP,(1,2),np.min([0.1, 1-RankCorP[1,2]]))
    TweakedLogRtnCorP = Tweak(LogRtnCorP,(1,2),np.min([0.1, 1-LogRtnCorP[1,2]]))
    TweakedRankCorP = Tweak(RankCorP,(0,4),np.min([0.1, 1-RankCorP[0,4]]))
    TweakedLogRtnCorP = Tweak(LogRtnCorP,(0,4),np.min([0.1, 1-LogRtnCorP[0,4]]))
    TweakedRankCorP = Tweak(RankCorP,(2,3),np.min([0.1, 1-RankCorP[2,3]]))
    TweakedLogRtnCorP = Tweak(LogRtnCorP,(2,3),np.min([0.1, 1-LogRtnCorP[2,3]]))
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t23 = FullMCFairSpreadValuation(plotter,t22,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Tweaked Correlation between Barclays & JPMorgan, Deutsche Bank & RBS and JPMorgan & Goldman Sachs by 0,1")
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Tweaked Correlation between Barclays & JPMorgan, Deutsche Bank & RBS and JPMorgan & Goldman Sachs by 0,1 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Tweaked Correlation between Barclays & JPMorgan, Deutsche Bank & RBS and JPMorgan & Goldman Sachs by 0,1 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])

    TweakedRankCorP = SetArbitrarily(RankCorP,(1,2),0.9)
    TweakedLogRtnCorP = SetArbitrarily(LogRtnCorP,(1,2),0.9)
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t24 = FullMCFairSpreadValuation(plotter,t23,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Correlation between Barclays & JPMorgan set to 0.9")
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Correlation between Barclays & JPMorgan set to 0.9 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Correlation between Barclays & JPMorgan set to 0.9 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])


    TweakedRankCorP = SetArbitrarily(RankCorP,(0,3),0.95)
    TweakedLogRtnCorP = SetArbitrarily(LogRtnCorP,(0,3),0.95)
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t25 = FullMCFairSpreadValuation(plotter,t24,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Correlation between Deutsche Bank & Goldman Sachs set to 0.95 ")
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Correlation between Deutsche Bank & Goldman Sachs set to 0.95 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Correlation between Deutsche Bank & Goldman Sachs set to 0.95 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])


    TweakedRankCorP = SetArbitrarily(RankCorP,(3,4),0.05)
    TweakedLogRtnCorP = SetArbitrarily(LogRtnCorP,(3,4),0.05)
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t26 = FullMCFairSpreadValuation(plotter,t25,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Correlation between Goldman Sachs & RBS set to 0.05")
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Correlation between Goldman Sachs & RBS set to 0.05 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Correlation between Goldman Sachs & RBS set to 0.05 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])

    TweakedRankCorP = SetArbitrarily(RankCorP,(0,3),-0.95)
    TweakedLogRtnCorP = SetArbitrarily(LogRtnCorP,(0,3),-0.95)
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t27 = FullMCFairSpreadValuation(plotter,t26,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Correlation between Deutsche Bank & Goldman Sachs set to -0.95")
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Correlation between Deutsche Bank & Goldman Sachs set to -0.95 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Correlation between Deutsche Bank & Goldman Sachs set to -0.95 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])

    TweakedRankCorP = SetWhole2DMatrixArbitrarily(RankCorP,-0.01)
    TweakedLogRtnCorP = SetWhole2DMatrixArbitrarily(LogRtnCorP,-0.01)
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t28 = FullMCFairSpreadValuation(plotter,t27,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Correlation between all reference names set to -0.01")
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Correlation between all reference names set to -0.01 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Correlation between all reference names set to -0.01 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])

    TweakedRankCorP = SetWhole2DMatrixArbitrarily(RankCorP,-0.99)
    TweakedLogRtnCorP = SetWhole2DMatrixArbitrarily(LogRtnCorP,-0.99)
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t29 = FullMCFairSpreadValuation(plotter,t28,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Correlation between all reference names set to -0.99")
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Correlation between all reference names set to -0.99 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Correlation between all reference names set to -0.99 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])

    TweakedRankCorP = SetWhole2DMatrixArbitrarily(RankCorP,0.99)
    TweakedLogRtnCorP = SetWhole2DMatrixArbitrarily(LogRtnCorP,0.99)
    FairSpreadGaussTweakCor,FairSpreadTTweakCor,t30 = FullMCFairSpreadValuation(plotter,t29,TweakedLogRtnCorP,TweakedRankCorP,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE[0],TenorCreditSpreads,InvPWCDF,
                                                                            DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,name="Correlation between all reference names set to 0.99")
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([GaussFairSpread,FairSpreadGaussTweakCor]),"Correlation between all reference names set to 0.99 (Gaussian)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])
    plotter.return_scatter_multdependencies(np.arange(1,6,1,dtype=np.int),np.array([TFairSpread,FairSpreadTTweakCor]),"Correlation between all reference names set to 0.99 (Students T)",xlabel="K-th to default",ylabel="Fair spread", legend=["Fair Spreads", "Tweaked Fair Spreads"])

results_bank=[]


def f(x):
    return x*x

print("Running outside of main thread flag")

if __name__ == '__main__':
    print("Running inside of main thread flag")

    fn_dict = dict()
    fns = [pCheckFairSpead,IRTweak,RTweak,AltHistDataTweak,AllFac10CreditAndHist,CreditSpreadTweak,CorTweaksAll,TweakPairwiseCors]
    for fn in fns:
        working_q.put(fn.__name__)
        fn_dict[fn.__name__] = fn

    processes = [mp.Process(target=worker,args=(working_q, output_q, fn_dict,plotter,M,HistCreditSpreads,SemiParamTransformedCDFHistDataDic, vE,
                        TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,
                        CDSPaymentTenors,CDSBasketMaturity, 
                        GaussFairSpread,TFairSpread,RankCorP,LogRtnCorP,ReferenceNameList,
                        TweakCDSSpreads,BPS_TO_NUMBER,
                        Fac10AltLogRtnCorP,Fac10AltRankCorP,Fac10AltSemiParamTransformedCDFHistDataDic,
                        AltLogRtnCorP,AltRankCorP,AltSemiParamTransformedCDFHistDataDic)) 
                for i in range(mp.cpu_count())]
    for i,p in enumerate(processes):
        print("Starting processes")
        p.start()
        print("started %d" %i)

    for i,p in enumerate(processes):
        print("joining %d" %i)
        p.join()

    while True:
       if output_q.empty() == True:
           break
       results_bank.append(output_q.get_nowait())
    print(results_bank)
    print("Program took: {0} minutes to finish, Enter any key to finish. (Debug Point)".format((time.time()-t1)/60))
    plotter.save_all_figs()

    print("Now the pool is closed and no longer available")


