import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from statsmodels.nonparametric.kde import KDEUnivariate
from statsmodels.nonparametric.kernel_density import KDEMultivariate
from scipy import stats
from scipy.stats.distributions import norm
from scipy.stats import genpareto
from EmpiricalFunctions import *
from Returns import mean, sd
import warnings
from bisect import bisect_left
from plotting import DualSortByL1

def takeIndexOfClosest(myList, myNumber):
    """
    Assumes myList is sorted. Returns closest index of value to myNumber.

    If two numbers are equally close, return the smallest number.
    """
    pos = bisect_left(myList, myNumber)
    if pos == 0:
        return myList[0]
    if pos == len(myList):
        return myList[-1]
    before = myList[pos - 1]
    after = myList[pos]
    if after - myNumber < myNumber - before:
       return after
    else:
       return before

def kde_statsmodels_u(x, x_grid, bandwidth=0.2, **kwargs):
    """Univariate Kernel Density Estimation with Statsmodels"""
    kde = KDEUnivariate(x)
    kde.fit(bw=bandwidth, **kwargs)
    return kde.evaluate(x_grid)

def kde_statsmodels_m_pdf(x, x_grid, bandwidth=0.2, **kwargs):
    """Multivariate Kernel Density Estimation with Statsmodels"""
    #kde = KDEMultivariate(x, bw=bandwidth * np.ones_like(x),
    #                      var_type='c', **kwargs)
    #! bw = "cv_ml", "cv_ls", "normal_reference", np.array([0.23])    
    pdf = kde_statsmodels_m_pdf_output(x, x_grid, bandwidth, **kwargs)
    x_grid_sorted = sorted(x_grid)
    def sub(x):
        def f(t):
            ind = int(takeIndexOfClosest(x_grid_sorted,t))
            if pdf[ind] <= t:
                return ((pdf[ind+1] - pdf[ind])/(x_grid_sorted[ind + 1] - x_grid_sorted[ind]))*(t - x_grid_sorted[ind]) if ind < len(pdf) else 0
            else:
                return ((pdf[ind] - pdf[ind-1])/(x_grid_sorted[ind] - x_grid_sorted[ind-1]))*(t-x_grid_sorted[ind-1]) if ind > 0 else 0
        m = np.fromiter(map(f,list(x)),dtype=np.float)
        return m[0] if len(m) == 1 else m
    return sub

def kde_statsmodels_m_pdf_output(x, x_grid, bandwidth=0.2, **kwargs):
    """Multivariate Kernel Density Estimation with Statsmodels"""
    #kde = KDEMultivariate(x, bw=bandwidth * np.ones_like(x),
    #                      var_type='c', **kwargs)
    #! bw = "cv_ml", "cv_ls", "normal_reference", np.array([0.23])
    kde = None
    while kde == None:
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore')
            try:
                kde = KDEMultivariate(data=x,var_type='c', bw="cv_ml")
                x_grid_sorted = sorted(x_grid)
                pdf = kde.pdf(x_grid_sorted)
            except Warning as e:
                print('error found:', e)
            warnings.filterwarnings('default')
    
    return pdf, kde.bw

def kde_statsmodels_m_cdf(x, x_grid, bandwidth=0.2, **kwargs):
    """Multivariate Kernel Cumulative Density Estimation with Statsmodels"""
    #kde = KDEMultivariate(x, bw=bandwidth * np.ones_like(x),
    #                      var_type='c', **kwargs)
    #! bw = "cv_ml", "cv_ls", "normal_reference", np.array([0.23])
    cdf = kde_statsmodels_m_cdf_output(x, x_grid, bandwidth, **kwargs)
    x_grid_sorted = sorted(x_grid)
    def sub(x):
        def f(t):
            ind = int(takeIndexOfClosest(x_grid_sorted,t))
            if cdf[ind] <= t:
                return ((cdf[ind+1] - cdf[ind])/(x_grid_sorted[ind + 1] - x_grid_sorted[ind]))*(t - x_grid_sorted[ind]) if ind < len(cdf) else 0
            else:
                return ((cdf[ind] - cdf[ind-1])/(x_grid_sorted[ind] - x_grid_sorted[ind-1]))*(t-x_grid_sorted[ind-1]) if ind > 0 else 0
        m = np.fromiter(map(f,list(x)),dtype=np.float)
        return m[0] if len(m) == 1 else m
    return sub

def kde_statsmodels_m_cdf_output(x, x_grid, bandwidth=0.2, **kwargs):
    """Multivariate Kernel Cumulative Density Estimation with Statsmodels"""
    #kde = KDEMultivariate(x, bw=bandwidth * np.ones_like(x),
    #                      var_type='c', **kwargs)
    #! bw = "cv_ml", "cv_ls", "normal_reference", np.array([0.23])
    kde = None
    while kde == None:
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore')
            try:
                kde = KDEMultivariate(data=x,var_type='c', bw="cv_ml")
                x_grid_sorted = sorted(x_grid)
                cdf = kde.cdf(x_grid_sorted)
            except Warning as e:
                print('error found:', e)
            warnings.filterwarnings('default')
    return cdf, kde.bw

    
    

def ReturnSemiParametricFitAndAFnToQueuePlot(plotter,c1,u,plotvsc1=False,name="Semi-Parametric Fit",xlabel="",ylabel=""):
    '''
    Return SemiParametricFit
    '''
    result,*args = SetUpSemiParametricCDFPlot(c1,u,plotvsc1,name,xlabel,ylabel)
    plotter.PlotSemiParametricFitResults(*args)

    return result 

def SetUpSemiParametricCDFPlot(c1,u,plotvsc1=False,name="Semi-Parametric Fit",xlabel="",ylabel=""):
    '''
    Calculates a SemiParametric fit to the data in c1.
    Uses a gaussian kernal estimation within the centre of the distribution of c1 which is decided by the threshold u.
    Uses a Generalised Pareto distribution to fit both tails outside of the threshold governed by u.
    Returns a tuple containing the the range (y points) of the (SemiPara-CDF,SemiPara-PDF); 
    if (plotvsc1 = False) => the y points depend on 1000 equally spaced points between min(c1) and max(c1).
    if (plotvsc1 = True) => the y points depend on the points in c1 and maintain the order of c1 in the outputted array. i.e. F_n(c1) where F_n is the semiparametric fitted function.
    '''
    #'https://mglerner.github.io/posts/histograms-and-kernel-density-estimation-kde-2.html?p=28'
    result = dict()
    us = list([u])
    x = np.linspace(min(c1), max(c1),1000) if plotvsc1 == False else c1
    i = 1
    for u in us:
        exceedances = list()
        internals = list()
        for rvs in c1:
            if abs(rvs) > u:
                exceedances.append(abs(rvs) - u)
            else:
                internals.append(rvs)
        fits = None
        while fits == None:
            with warnings.catch_warnings():
                warnings.filterwarnings('ignore')
                try:
                    fits = genpareto.fit(exceedances)
                except Warning as e:
                    print('error found:', e)
                warnings.filterwarnings('default')
        internals = np.array(internals).reshape((len(internals),1))
        
        r1c,r2c,r3c,r4c, bwArr_Cdf = HybridSemiParametricGPDCDF(x,u,c1,fits[0],loc=fits[1],scale=fits[2])
        emp = pd.Series(r1c).apply(Empirical_StepWise_CDF(sorted(c1)))
        r1s,r2cs = DualSortByL1(r1c,r2c)
        
        r1p,r2p,r3p,r4p, bwArr_pdf = HybridSemiParametricGPDPDF(x,u,c1,fits[0],loc=fits[1],scale=fits[2])
        r1s,r2ps = DualSortByL1(r1p,r2p)

        result['%.10f'%(u)] = (r2c,r2p)
        i += 3

        plt.subplots_adjust(hspace=0.48)
    return result, c1,u,r1c,r2c,r3c,r4c,bwArr_Cdf,r1p,r2p,r3p,r4p,bwArr_pdf,plotvsc1,name,xlabel,ylabel 



def HybridNormalGPDCDF(xs, u, mu, sigma, shape, loc, scale):
    '''
    Params: 
        xs: unsorted list of datat to fit semi-parametric CDF to.
        u: threshold to move from Gaussian CDF Fit in center to GPD tail fitting.
        mu:  mean of the data.
        sigma: standard deviation of the data.
        shape: gpd least squares estimated shape parameter.
        loc: gpd least squares estimated location parameter.
        scale: gpd least squares estimated scale parameter.
    Returns:
        an array that would result from xs.apply(semiparametric_fittedfunction) or F_n(xs) where F_n is the CDF fit.
    '''
    out = list()
    l = (mu - abs(u - mu))
    h = (mu + abs(u - mu))
    
    for x in xs:
        if x < l:
            nrm = norm.cdf(l,mu,sigma)
            out.append(nrm*(1-genpareto.cdf(l - x, shape, loc=loc, scale=scale)))
        elif x >= h:
            nrm = norm.cdf(h,mu,sigma)
            out.append((1 - nrm)*genpareto.cdf(x - h, shape, loc=loc, scale=scale) + nrm)
        else:
            out.append(norm.cdf(x,mu,sigma))
    return out

def HybridNormalGPDPDF(xs, u, mu, sigma, shape, loc, scale):
    '''
    Params: 
        xs: unsorted list of datat to fit semi-parametric PDF to.
        u: threshold to move from Gaussian PDF Fit in center to GPD tail fitting.
        mu:  mean of the data.
        sigma: standard deviation of the data.
        shape: gpd least squares estimated shape parameter.
        loc: gpd least squares estimated location parameter.
        scale: gpd least squares estimated scale parameter.
    Returns:
        an array that would result from xs.apply(semiparametric_fittedfunction) or F_n(xs) where F_n is the PDF fit.
    '''
    out = list()
    l = (mu - abs(u - mu))
    h = (mu + abs(u - mu))
    
    for x in xs:
        if x < l:
            out.append(norm.cdf(l,mu,sigma)*genpareto.pdf(l-x,shape, loc=loc, scale=scale))
        elif x >= h:
            out.append((1 - norm.cdf(h,mu,sigma))*genpareto.pdf(x-h, shape, loc=loc, scale=scale))
        else:
            out.append(norm.pdf(x,mu,sigma))
    return out

def HybridSemiParametricGPDCDF(xs, u, ydata, shape, loc, scale):
    '''
    Params: 
        xs: unsorted list of datat to fit semi-parametric CDF to.
        u: threshold to move from Gaussian Kernel estimation to GPD tail fitting.
        mu:  mean of the data.
        sigma: standard deviation of the data.
        shape: gpd least squares estimated shape parameter.
        loc: gpd least squares estimated location parameter.
        scale: gpd least squares estimated scale parameter.
    Returns:
        an array that would result from xs.apply(semiparametric_fittedfunction) or F_n(xs) where F_n is the CDF fit.
    '''
    
    out = list()
    mu = mean(ydata)
    l = (mu - abs(u - mu))
    h = (mu + abs(u - mu))
    
    srtdxs = sorted(list(xs)+[l,h])
    bandwidth = 0.2
    cdf_smoother, bandwidth = kde_statsmodels_m_cdf_output(ydata,srtdxs,bandwidth=bandwidth)
    d = dict(zip(srtdxs,cdf_smoother))
    
    for x in xs:
        if x < l:
            nrm = d[l]
            out.append(nrm*(1-genpareto.cdf(l - x, shape, loc=loc, scale=scale)))
        elif x >= h:
            nrm = d[h]
            out.append((1 - nrm)*genpareto.cdf(x - h, shape, loc=loc, scale=scale) + nrm)
        else:
            out.append(d[x])
    return xs,out,srtdxs,cdf_smoother,bandwidth

def HybridSemiParametricGPDPDF(xs, u, ydata, shape, loc, scale):
    '''
    Params: 
        xs: unsorted list of datat to fit semi-parametric PDF to.
        u: threshold to move from Gaussian Kernel estimation to GPD tail fitting.
        mu:  mean of the data.
        sigma: standard deviation of the data.
        shape: gpd least squares estimated shape parameter.
        loc: gpd least squares estimated location parameter.
        scale: gpd least squares estimated scale parameter.
    Returns:
        an array that would result from xs.apply(semiparametric_fittedfunction) or F_n(xs) where F_n is the PDF fit.
    '''
    out = list()
    mu = mean(ydata)
    l = (mu - abs(u - mu))
    h = (mu + abs(u - mu))
    
    bandwidth = 0.2
    srtdxs = sorted(list(xs)+[l,h])
    cdf_smoother, bandwidth = kde_statsmodels_m_cdf_output(ydata,srtdxs,bandwidth=bandwidth)
    d_cdf = dict(zip(srtdxs,cdf_smoother))
    pdf_smoother, bandwidth = kde_statsmodels_m_pdf_output(ydata,srtdxs,bandwidth=bandwidth)
    d_pdf = dict(zip(srtdxs,pdf_smoother))
    for x in xs:
        if x < l:
            out.append(d_cdf[l]*genpareto.pdf(l-x,shape, loc=loc, scale=scale))
        elif x >= h:
            out.append((1 - d_cdf[h])*genpareto.pdf(x-h, shape, loc=loc, scale=scale))
        else:
            out.append(d_pdf[x])
    return xs,out,srtdxs,pdf_smoother,bandwidth


