import numpy as np
cimport numpy as np

def CumAverage(np.ndarray arr):
	cdef int i
	for i in range(1,len(arr)):
		arr[i] = ((arr[i-1]*(i)+arr[i])/(i+1))
	return arr