import math
import numpy as np
from LowDiscrepancyNumberGenerators import SobolNumbers
import time
import queue
import threading
from multiprocessing.pool import ThreadPool
from SimulateLegs import SimulateLegPricesFromCorrelationNormal, SimulateLegPricesFromCorrelationT, UnifFromGaussCopula, UnifFromTCopula
from RunningMoments import RunningAverage, RunningVarianceOfRunningAverage
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!MONTE CARLO SIMULATION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

pool = ThreadPool(processes=1)

def SimulateCDSBasketDefaultsAndValueLegsT(Plotter,TimeAtStart,CorP,M,HistCreditSpreads,TransformedHistDataDic,T_df,TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,SpreadArr=[],R=0.4,name="",plot=False):
    if len(SpreadArr) == 0:
        SpreadArr = np.ones(shape=(5))
    CompensationLegSumsForEachRefNameT = np.zeros(shape=(M,5),dtype=np.float)
    PremiumLegSumsForEachRefNameT = np.zeros(shape=(M,5),dtype=np.float)
    NumbGen = SobolNumbers()
    NumbGen.initialise(CorP.shape[0])
    UT = UnifFromTCopula(CorP,NumbGen,T_df,M)
    UniformityDic = dict()
    for i in range(2,4):
        UniformityDic["T Copula %d" % (i+1)] = UT[i,:]
    if plot:
        Plotter.lock()
        Plotter.plot_histogram_array(UniformityDic,"Simulated Ui T Copula",name=name)
        Plotter.plot_codependence_scatters(UniformityDic,"Simulated Ui T Copula", "Simulated Uj T Copula",name)
        Plotter.unlock()
        Plotter.save_all_figs()
    M_Min = 100
    Tolerance = 0.000001
    CompLegRunningAv = np.zeros(shape=(M,5))
    PremLegRunningAv = np.zeros(shape=(M,5))
    runningVarCompLeg = []
    runningVarPremLeg = []
 #   out_results = []

 #   def RunMonteCarlo(InnerM,baseM,CompensationLegSumsForEachRefNameT,PremiumLegSumsForEachRefNameT,PremLegRunningAv,CompLegRunningAv,runningVarCompLeg,runningVarPremLeg,out_results):
 #       for m in range(baseM+1,baseM+InnerM+1):
 #           TLegsSum = SimulateLegPricesFromCorrelationT(HistCreditSpreads,TenorCreditSpreads,CDSPaymentTenors,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,UT[:,m-1],int(CDSBasketMaturity),SpreadArr,R,0.25)
 #           CompensationLegSumsForEachRefNameT[m-1] = [TLegsSum[1][0],TLegsSum[2][0],TLegsSum[3][0],TLegsSum[4][0],TLegsSum[5][0]]
 #           PremiumLegSumsForEachRefNameT[m-1] = [TLegsSum[1][1],TLegsSum[2][1],TLegsSum[3][1],TLegsSum[4][1],TLegsSum[5][1]]
            
 #           xd = CompLegRunningAv.shape[0]
 #           CompLegRunningAv = np.concatenate((CompLegRunningAv, [((CompLegRunningAv[xd-1,:] * (xd)) + CompensationLegSumsForEachRefNameT[m-1]) / (xd+1)]), axis=0)
 #           PremLegRunningAv = np.concatenate((PremLegRunningAv, [((PremLegRunningAv[xd-1,:] * (xd)) + PremiumLegSumsForEachRefNameT[m-1]) / (xd+1)]), axis=0)
 #           if (m-1) % M_Min == 0 and m > M_Min + baseM:
 #               runningVarCompLeg = RunningVarianceOfRunningAverage(np.array(CompLegRunningAv[:,:]).transpose(),M_Min)
 #               runningVarPremLeg = RunningVarianceOfRunningAverage(np.array(PremLegRunningAv[:,:]).transpose(),M_Min)
 #               supRunningVarsCompLeg = max(runningVarCompLeg.transpose()[runningVarCompLeg.shape[1]-1])
 #               supRunningVarsPremLeg = max(runningVarPremLeg.transpose()[runningVarPremLeg.shape[1]-1])
 #               sup = max(supRunningVarsCompLeg,supRunningVarsPremLeg)
 #               if sup < Tolerance:
 #                   print("Tolerance hit for T")
 #                   out_results.append((runningVarCompLeg, runningVarPremLeg))
 #                   break
 #               elif (m-1+M_Min) > baseM+InnerM:
 #                   out_results.insert(0,(runningVarCompLeg, runningVarPremLeg)) 
 #   threads = 1   
 #   jobs = []
 #   innerM = round(M/threads)
 #   for r in range(0,threads):
 #       thread = threading.Thread(target=RunMonteCarlo(innerM,r*innerM,CompensationLegSumsForEachRefNameT,PremiumLegSumsForEachRefNameT,
 #                                                      CompLegRunningAv,PremLegRunningAv,runningVarCompLeg,runningVarPremLeg,out_results))
 #       jobs.append(thread)

	## Start the threads (i.e. calculate the random number lists)
 #   for j in jobs:
 #       j.start()
    
 #   # Ensure all of the threads have finished
 #   for j in jobs:
 #       j.join()

 #   runningVarCompLeg = out_results[-1][0]
 #   runningVarPremLeg = out_results[-1][1]
    #Python is already multithreaded so introducing more threads here slows the processes down.
    for m in range(1,M):
        #Generate new legs sums
        TLegsSum = SimulateLegPricesFromCorrelationT(HistCreditSpreads,TenorCreditSpreads,CDSPaymentTenors,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,UT[:,m-1],int(CDSBasketMaturity),SpreadArr,R,0.25)
        #store results
        CompensationLegSumsForEachRefNameT[m-1] = [TLegsSum[1][0],TLegsSum[2][0],TLegsSum[3][0],TLegsSum[4][0],TLegsSum[5][0]]
        PremiumLegSumsForEachRefNameT[m-1] = [TLegsSum[1][1],TLegsSum[2][1],TLegsSum[3][1],TLegsSum[4][1],TLegsSum[5][1]]
        #calculate running average
        CompLegRunningAv[m-1,:] = ((CompLegRunningAv[m-2,:] * (m-1)) + CompensationLegSumsForEachRefNameT[m-1]) / (m)
        PremLegRunningAv[m-1,:] = ((PremLegRunningAv[m-2,:] * (m-1)) + PremiumLegSumsForEachRefNameT[m-1]) / (m)
        if (m-1) % M_Min == 0 and m > M_Min:
            runningVarCompLeg = RunningVarianceOfRunningAverage(np.array(CompLegRunningAv[0:(m-1),:]).transpose(),M_Min)
            runningVarPremLeg = RunningVarianceOfRunningAverage(np.array(PremLegRunningAv[0:(m-1),:]).transpose(),M_Min)
            supRunningVarsCompLeg = max(runningVarCompLeg.transpose()[runningVarCompLeg.shape[1]-1])
            supRunningVarsPremLeg = max(runningVarPremLeg.transpose()[runningVarPremLeg.shape[1]-1])
            sup = max(supRunningVarsCompLeg,supRunningVarsPremLeg)
            if sup < Tolerance:
                print("Tolerance hit for T")
                break

    if plot:
        Plotter.lock()
        Plotter.return_lineChart(np.arange(0,M),CompLegRunningAv.transpose(),name+"_"+"Compensation Leg Running Average (Student's T Copula)",xlabel="Iteration",ylabel="Running Average", legend=["1st to Default","2nd to Default","3rd to Default","4th to Default","5th to Default"],numberPlot=1,noOfPlotsW=2,noOfPlotsH=2,trimTrailingZeros=True)
        Plotter.return_lineChart(np.arange(0,M),PremLegRunningAv.transpose(),name+"_"+"Premium Leg Running Average (Student's T Copula)",xlabel="Iteration",ylabel="Running Average", legend=["1st to Default","2nd to Default","3rd to Default","4th to Default","5th to Default"],numberPlot=2,noOfPlotsW=2,noOfPlotsH=2,trimTrailingZeros=True)
        Plotter.return_lineChart(np.arange(0,runningVarCompLeg.shape[1]),runningVarCompLeg,name+"_"+"Running Variance of Compensation Leg (Student's T Copula)",xlabel="Iteration",ylabel="Running Variance",legend=["1st to Default","2nd to Default","3rd to Default","4th to Default","5th to Default"],numberPlot=3,noOfPlotsW=2,noOfPlotsH=2,trimTrailingZeros=True)
        Plotter.return_lineChart(np.arange(0,runningVarCompLeg.shape[1]),runningVarPremLeg,name+"_"+"Running Variance of Premium Leg (Student's T Copula)",xlabel="Iteration",ylabel="Running Variance",legend=["1st to Default","2nd to Default","3rd to Default","4th to Default","5th to Default"],numberPlot=4,noOfPlotsW=2,noOfPlotsH=2,trimTrailingZeros=True)
        Plotter.unlock()
        Plotter.save_all_figs()
    tEnd = time.time()
    print("Took %.10f seconds to simulate %d iterations of T copulae and calculate legs from them." % (tEnd - TimeAtStart,M))

    return CompensationLegSumsForEachRefNameT,PremiumLegSumsForEachRefNameT,tEnd

def SimulateCDSBasketDefaultsAndValueLegsGauss(Plotter,TimeAtStart,CorP,M,HistCreditSpreads,TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,SpreadArr=[],R=0.4,name="",plot=False):
    if len(SpreadArr) == 0:
        SpreadArr = np.ones(shape=(5))
    CompensationLegSumsForEachRefNameGauss = np.zeros(shape=(M,5),dtype=np.float)
    PremiumLegSumsForEachRefNameGauss = np.zeros(shape=(M,5),dtype=np.float)
    NumbGen = SobolNumbers()
    NumbGen.initialise(CorP.shape[0])
    UNorm = UnifFromGaussCopula(CorP,NumbGen,M)
    UniformityDic = dict()
    for i in range(0,2):
        UniformityDic["Gaussian Copula %d" % (i+1)] = UNorm[i,:]
    if plot:
        Plotter.lock()
        Plotter.plot_histogram_array(UniformityDic,"Simulated Ui Gaussian Copula",name=name)
        Plotter.plot_codependence_scatters(UniformityDic,"Simulated Ui Gaussian Copula","Simulated Uj Gaussian Copula",name)
        Plotter.unlock()
        Plotter.save_all_figs()
    M_Min = 100
    Tolerance = 0.000001
    CompLegRunningAv = np.zeros(shape=(M,5))
    PremLegRunningAv = np.zeros(shape=(M,5))
    for m in range(1,M):
        #Generate new legs sums
        GaussLegsSum = SimulateLegPricesFromCorrelationNormal(HistCreditSpreads,TenorCreditSpreads,CDSPaymentTenors,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,UNorm[:,m-1],int(CDSBasketMaturity),SpreadArr,R,0.25)
        #store results
        CompensationLegSumsForEachRefNameGauss[m-1] = [GaussLegsSum[1][0],GaussLegsSum[2][0],GaussLegsSum[3][0],GaussLegsSum[4][0],GaussLegsSum[5][0]]
        PremiumLegSumsForEachRefNameGauss[m-1] = [GaussLegsSum[1][1],GaussLegsSum[2][1],GaussLegsSum[3][1],GaussLegsSum[4][1],GaussLegsSum[5][1]]
        #calculate running average
        CompLegRunningAv[m-1,:] = ((CompLegRunningAv[m-2,:] * (m-1)) + CompensationLegSumsForEachRefNameGauss[m-1]) / (m)
        PremLegRunningAv[m-1,:] = ((PremLegRunningAv[m-2,:] * (m-1)) + PremiumLegSumsForEachRefNameGauss[m-1]) / (m)
        if (m-1) % M_Min == 0 and m > M_Min:
            #calculate running variance
            runningVarCompLeg = RunningVarianceOfRunningAverage(np.array(CompLegRunningAv[0:(m-1),:]).transpose(),M_Min)
            runningVarPremLeg = RunningVarianceOfRunningAverage(np.array(PremLegRunningAv[0:(m-1),:]).transpose(),M_Min)
            supRunningVarsCompLeg = max(runningVarCompLeg.transpose()[runningVarCompLeg.shape[1]-1])
            supRunningVarsPremLeg = max(runningVarPremLeg.transpose()[runningVarPremLeg.shape[1]-1])
            sup = max(supRunningVarsCompLeg,supRunningVarsPremLeg)
            #stop simulation if simulation variance is below a predefined tolerance
            if(sup < Tolerance):
                print("Tolerance hit for Gaussian")
                break
    if plot:
        Plotter.lock()
        Plotter.return_lineChart(np.arange(0,M),CompLegRunningAv.transpose(),name+"_"+"Compensation Leg Running Average (Gaussian Copula)",xlabel="Iteration",ylabel="Running Average", legend=["1st to Default","2nd to Default","3rd to Default","4th to Default","5th to Default"],numberPlot=1,noOfPlotsW=2,noOfPlotsH=2,trimTrailingZeros=True)
        Plotter.return_lineChart(np.arange(0,M),PremLegRunningAv.transpose(),name+"_"+"Premium Leg Running Average (Gaussian Copula)",xlabel="Iteration",ylabel="Running Average", legend=["1st to Default","2nd to Default","3rd to Default","4th to Default","5th to Default"],numberPlot=2,noOfPlotsW=2,noOfPlotsH=2,trimTrailingZeros=True)
        Plotter.return_lineChart(np.arange(0,len(runningVarCompLeg[0])),runningVarCompLeg,name+"_"+"Running Variance of Compensation Leg (Gaussian Copula)",xlabel="Iteration",ylabel="Running Variance",legend=["1st to Default","2nd to Default","3rd to Default","4th to Default","5th to Default"],numberPlot=3,noOfPlotsW=2,noOfPlotsH=2,trimTrailingZeros=True)
        Plotter.return_lineChart(np.arange(0,len(runningVarCompLeg[0])),runningVarPremLeg,name+"_"+"Running Variance of Premium Leg (Gaussian Copula)",xlabel="Iteration",ylabel="Running Variance",legend=["1st to Default","2nd to Default","3rd to Default","4th to Default","5th to Default"],numberPlot=4,noOfPlotsW=2,noOfPlotsH=2,trimTrailingZeros=True)
        Plotter.unlock()
        Plotter.save_all_figs()
    tEnd = time.time()
    print("Took %.10f seconds to simulate %d iterations of Gaussian copulae and calculate legs from them." % (tEnd - TimeAtStart,M))

    return CompensationLegSumsForEachRefNameGauss,PremiumLegSumsForEachRefNameGauss,tEnd

def CalculateFairSpreadFromLegs(CompensationLegSumsForEachRefName,PremiumLegSumsForEachRefName,M,t8,CopulaClass=""):
    CompAv = dict()
    PremAv = dict()
    CompRunningAv = np.zeros(shape=(5+1,M),dtype=np.float)
    PremRunningAv = np.zeros(shape=(5+1,M),dtype=np.float)

    for i in range(1,6):
        CompAv[i] = sum([item[i-1] for item in CompensationLegSumsForEachRefName]) / len(CompensationLegSumsForEachRefName)
        PremAv[i] = sum([item[i-1] for item in PremiumLegSumsForEachRefName]) / len(PremiumLegSumsForEachRefName)

    FairSpreads = np.zeros(shape=(5),dtype=np.float)

    for i in range(0,5):
        FairSpreads[i] = CompAv[i+1] / PremAv[i+1]

    t9 = time.time()
    print("Took %.10f seconds to print running statistics and calculate fair spreads from legs for %s Copula." % (t9 - t8,CopulaClass))

    return FairSpreads, t9

def FullMCFairSpreadValuation(Plotter,startTime,LogRtnCorP,RankCorP,M,HistCreditSpreads,TransformedHistDataDic,T_df,TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,R=0.4,name="",plot=False):

    TLegs = SimulateCDSBasketDefaultsAndValueLegsT(Plotter,startTime,RankCorP,M,HistCreditSpreads,TransformedHistDataDic,T_df,TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,[],R,name,plot)
    GaussLegs = SimulateCDSBasketDefaultsAndValueLegsGauss(Plotter,TLegs[2],LogRtnCorP,M,HistCreditSpreads,TenorCreditSpreads,InvPWCDF,DiscountFactorCurve,ImpHazdRts,DataTenorDic,CDSPaymentTenors,CDSBasketMaturity,[],R,name,plot)

    

    GaussFairSpread,t9 = CalculateFairSpreadFromLegs(GaussLegs[0],GaussLegs[1],M,GaussLegs[2],"Gauss")
    TFairSpread,t10 = CalculateFairSpreadFromLegs(TLegs[0],TLegs[1],M,t9,"T")
    tEnd = time.time()
    
    print("Took {0} secs to run FullMCSpead Analysis".format(tEnd-startTime))

    return GaussFairSpread, TFairSpread, t10